-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2020 at 04:39 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `nts`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AGRICULTURAL SOLUTION', 'The best agricultural solution', '1781672008slider1.png', 1, '2020-09-28 13:01:52', '2020-09-28 13:01:52'),
(2, 'DRIP WATER IRRIGATION', 'Nepal’s leading irrigation company, we will drive mass adoption of smart irrigation solutions to fight scarcity of food, water and land.', '1136787379slider2.png', 1, '2020-09-28 13:03:06', '2020-09-28 13:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `sort`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Green House', 'green-house', 1, 1, 'Green House', '2020-09-29 07:40:54', '2020-09-29 07:40:54'),
(2, 'Drip/Sprinkler Irrigation', 'dripsprinkler-irrigation', 2, 1, 'Drip/Sprinkler Irrigation', '2020-09-29 07:41:23', '2020-09-29 07:41:23'),
(3, 'Kaushi kheti', 'kaushi-kheti', 3, 1, 'Kaushi kheti', '2020-09-29 07:41:34', '2020-09-29 07:41:34'),
(4, 'Solar/Electric Pumping', 'solarelectric-pumping', 4, 1, 'Solar/Electric Pumping', '2020-09-29 07:41:43', '2020-09-29 07:41:43'),
(5, 'Agriculture Machinery', 'agriculture-machinery', 5, 1, 'Agriculture Machinery', '2020-09-29 07:41:53', '2020-09-29 07:41:53'),
(6, 'Agriculture Tools', 'agriculture-tools', 6, 1, 'Agriculture Tools', '2020-09-29 07:42:05', '2020-09-29 07:42:05'),
(7, 'Others', 'others', 7, 1, 'Others', '2020-09-29 07:42:47', '2020-09-29 07:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, '1353695373product2.jpg', '2020-09-29 08:24:01', '2020-09-29 08:24:01'),
(2, '21229500product3.jpg', '2020-09-29 08:24:01', '2020-09-29 08:24:01'),
(3, '372693324product1.jpg', '2020-09-29 08:24:01', '2020-09-29 08:24:01'),
(4, '2026197575gallery-2-img-3.jpg', '2020-09-29 08:25:01', '2020-09-29 08:25:01'),
(5, '577150110gallery-2-img-1.jpg', '2020-09-29 08:25:01', '2020-09-29 08:25:01'),
(6, '734873858gallery-2-img-6.jpg', '2020-09-29 08:25:01', '2020-09-29 08:25:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(23, '2014_10_12_000000_create_users_table', 1),
(24, '2014_10_12_100000_create_password_resets_table', 1),
(25, '2019_08_19_000000_create_failed_jobs_table', 1),
(26, '2020_09_25_093754_create_categories_table', 1),
(27, '2020_09_26_070110_create_products_table', 1),
(28, '2020_09_27_085450_create_banners_table', 1),
(29, '2020_09_27_153246_create_galleries_table', 1),
(30, '2020_09_28_050624_create_settings_table', 1),
(31, '2020_09_28_052708_create_socials_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `image`, `category_id`, `status`, `description`, `created_at`, `updated_at`) VALUES
(1, 'abc', 'abc', '900440332product1.jpg', 1, 1, 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-29 07:45:54', '2020-09-29 21:46:57'),
(2, 'Product 2', 'product-2', '1012922510product2.jpg', 1, 1, 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-29 07:46:23', '2020-09-29 07:46:23'),
(3, 'Product 3', 'product-3', '946245294product3.jpg', 1, 1, 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-29 07:47:20', '2020-09-29 07:47:20'),
(4, 'Product 4', 'product-4', '1859274877product1.jpg', 1, 1, 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-29 08:13:15', '2020-09-29 08:13:15'),
(5, 'Product 5', 'product-5', '1389970109product2.jpg', 1, 1, 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-29 08:13:40', '2020-09-29 08:13:40'),
(6, 'Product 6', 'product-6', '536893948product3.jpg', 1, 1, 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-29 08:14:10', '2020-09-29 08:14:10');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `choose` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `email`, `phone`, `address`, `about`, `choose`, `created_at`, `updated_at`) VALUES
(1, '426635658nts_logo.png', 'info@nepalthopasinchai.com.np', '+977-9812131415', 'Kalanki-14 Kathmandu', 'NTS was established with a campaign of TRUST IN AGRICULTURE through the supply of modern agricultural and smart irrigation services for the farmers and institution working for the technology based modern agriculture.', 'There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable. There are many variations of passages of available but the majority have suffered alteration in some form, by injected humou or randomised words which don\'t look even slightly believable.', '2020-09-28 12:52:45', '2020-09-28 13:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `social` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `social`, `created_at`, `updated_at`) VALUES
(31, 'https://www.facebook.com', '2020-09-28 11:54:37', '2020-09-28 11:54:37'),
(32, 'https://www.youtube.com', '2020-09-28 12:49:32', '2020-09-28 12:49:32'),
(34, 'https://www.instagram.com', '2020-09-28 13:17:01', '2020-09-28 13:17:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$OBaq2Hz707wCGhs63wPWnu5bLJOaQmrzdmngF6ikudJupOxl92Gf2', NULL, '2020-09-29 13:21:16', '2020-09-29 13:21:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;
