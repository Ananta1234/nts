<footer class="site-footer">
    <div class="site-footer_farm_image"><img src="/frontend/assets/images/resources/site-footer-farm.png"
        alt="Farm Image"></div>
        <div class="container">
            <div class="row no-gutters">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="footer-widget__column footer-widget__about wow fadeInUp" data-wow-delay="100ms">
                        <div class="footer-widget__title">
                            <h3>About</h3>
                        </div>
                        <div class="footer-widget_about_text">
                            <p>{{\Str::limit(getSetting()->about,220,'...')}}</p>
                        </div>
                       
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6">
                    <div class="footer-widget__column footer-widget__link wow fadeInUp" data-wow-delay="200ms">
                        <div class="footer-widget__title">
                            <h3>Explore</h3>
                        </div>
                        <ul class="footer-widget__links-list list-unstyled">
                            <li><a href="#">Home</a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Category</a></li>
                            <li><a href="">Videos</a></li>
                            <li><a href="">Gallery</a></li>
                            <li><a href="">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-widget__column footer-widget__contact wow fadeInUp" data-wow-delay="400ms">
                        <div class="footer-widget__title">
                            <h3>Contact</h3>
                        </div>
                        <div class="footer-widget_contact">
                            <p>{{getSetting()->address}}</p>
                            <a href="mailto:needhelp@agrikol.com">{{getSetting()->email}}</a><br>
                            <a href="tel:666-888-0000">{{getSetting()->phone}}</a>
                            <div class="site-footer__social">
                                @foreach(getSocialIcon() as $icon=>$url)
                                    
                                <a href="{{$url}}" target="_blank"><i class="fab fa-{{$icon}}"></i></a>
                              
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-widget__column footer-widget__news wow fadeInUp" data-wow-delay="300ms">
                        <div class="footer-widget__title">
                            <h3>Facebook Page</h3>
                        </div>
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FNTSAgroSolutions&tabs=timeline&width=340&height=331&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=354128759049850" width="100%" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

                    </div>
                </div>

            </div>
        </div>
    </footer>

    <div class="site-footer_bottom">
        <div class="container">
            <div class="site-footer_bottom_copyright">
                <p>@ All copyright 2020, <a href="#">Momtech Nepal</a></p>
            </div>
        </div>
    </div>




</div><!-- /.page-wrapper -->


<a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


<div class="side-menu__block">
    <div class="side-menu__block-overlay custom-cursor__overlay">
        <div class="cursor"></div>
        <div class="cursor-follower"></div>
    </div><!-- /.side-menu__block-overlay -->
    <div class="side-menu__block-inner ">
        <div class="side-menu__top justify-content-end">
            <a href="#" class="side-menu__toggler side-menu__close-btn"><img
                src="assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->

            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>

            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->

            <div class="side-menu__content">
                <p><a href="mailto:needhelp@tripo.com">info@nepalthopasinchai.com.np</a> <br> <a href="tel:888-999-0000">+977-9812131415</a></p>
                <div class="side-menu__social">
                    <a href="#"><i class="fab fa-facebook-square"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-pinterest-p"></i></a>
                </div>
            </div>
        </div>
    </div>
