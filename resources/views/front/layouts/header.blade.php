
<div class="site-header__header-one-wrap">


  <header class="main-nav__header-one">
    <nav class="header-navigation stricky">
       <div class="search-box">
              <div class="container">
                 <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-7" style="padding-right:0px">
                      <form method="post" class="search-form" action="{{route('front.search')}}">@csrf
                          <input type="text" name="product" class="form-control" placeholder="Search Product">
                          <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                 </div>
              </div>
        </div>
      <div class="container clearfix">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="main-nav__left">
          <div class="mb-flex">
            <div class="logo mb-left">
              <img src="/images/logo/{{getSetting()->logo}}" style="width:100px">
            </div>
            <div class="mb-right">
              <a href="#" class="side-menu__toggler">
                <i class="fa fa-bars"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="main-nav__main-navigation">
            
                 <ul class="main-nav__navigation-box">
                      <li class="current">
                        <a href="{{route('front.index')}}">Home</a>
                      </li>
                      <li><a href="{{route('front.about')}}">About Us</a></li>
                      <li class="dropdown">
                        <a href="projects.html">Category<button class="dropdown-btn"><i class="fa fa-angle-right"></i></button></a>
                        <ul>
                         @foreach(getOrderCategoryActive() as $category)
                         <li><a href="{{route('front.product',$category->slug)}}">{{$category->name}}</a></li>
                         @endforeach


                       </ul><!-- /.sub-menu -->
                     </li>
                      @php
                        foreach(getSocialIcon() as $icon=>$url)
                        {
                          if($icon=='youtube')
                          {
                            $youtubeUrl=$url;
                            break;
                          }
                        }
                      @endphp

                     <li><a href="{{$youtubeUrl}}" target="_blank">Videos</a></li>
                     <li><a href="{{route('front.gallery')}}">Gallery</a></li>
                     <li><a href="{{route('front.contact')}}">Contact</a></li>

                   </li>                
                 </ul>
          </div>
         <div class="main-nav__right">
          <div class="icon_cart_box">
            <a href="{{route('front.cart')}}">
              <span class="icon-shopping-cart"></span>
            </a>
          </div>
  </div>

</div>
</nav>
</header>
</div>