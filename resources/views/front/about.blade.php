@extends('front.layouts.default')
@section('content')
<section class="page-header" style="background-image: url(/frontend/assets/images/backgrounds/page-header-contact.jpg);">
	<div class="container">
		<h2>About Us</h2>
		<ul class="thm-breadcrumb list-unstyled">
			<li><a href="index-2.html">Home</a></li>
			<li><span>About Us</span></li>
		</ul>
	</div>
</section>
   

 <section class="about_two">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="block-title text-left">
                            <p>About agrikol</p>
                            <h3>We’re Providing The Best Solution</h3>
                            <div class="leaf">
                                <img src="assets/images/resources/leaf.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="about_two_text">
                            <p>{{getSetting()->about}}</p>
                        </div>
                    </div>
                </div>
              
            </div>
        </section>
       
@endsection