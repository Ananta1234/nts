@extends('front.layouts.default')
@section('content')
<!-- Banner Section -->
<section class="banner-section banner-one">

    <div class="banner-carousel owl-theme owl-carousel">
        @foreach($data['banner'] as $item)
        <div class="slide-item">
            <div class="image-layer" style="background-image: url({{asset('images/banners/'.$item->image)}});">
            </div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title">{{$item->description}}</div>
                            <h1>{{$item->title}}</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slide Item -->
      <!--   <div class="slide-item">
            <div class="image-layer" style="background-image: url({{asset('frontend/assets/img/slider/slider2.png')}});">
            </div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title">The best Agriculture products</div>
                            <h1>AGRICULTURAL SOLUTION</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        @endforeach
    </div>
</section>

<section class="recent-project">
    <div class="container">
        <div class="block-title text-center">
            <p>Nepal Thopa Sinchai</p>
            <h3>Latest Product</h3>

        </div>
        @foreach(getOrderCategoryActive() as $category)
           @if($category->frontProducts()->count() > 0)
            <div class="row">
                <div class="col-md-3">
                    <div class="category-name">
                        <h5>{{$category->name}}</h5>
                    </div>
                </div>
               <div class="col-md-9">
                   <div class="row">
                        @foreach($category->frontProducts() as $product)
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="recent_project_single wow fadeInUp animated" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                                <div class="project_img_box">
                                    <img src="{{asset('images/products/'.$product->image)}}">

                                </div>
                                <div class="blog-one__content">

                                    <h3><a href="{{route('front.productdetails',$product->slug)}}">{{$product->name}}</a></h3>

                                    <div class="read_more_btn">
                                        <form action="{{route('front.cartStore')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <button type="submit"><i class="fa fa-shopping-cart"></i>Order Now</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                   </div>
               </div>
            </div>
            @endif
        @endforeach

    </div>
</section>
<section class="benefits">
    <div class="benefits_bg" style="background-image: url({{asset('frontend/assets/images/resources/benifits_bg.png')}})"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div class="block-title text-left">
                    <p>Our Values</p>
                    <h3>Why Choose Us ?</h3>

                </div>
            </div>
            <div class="col-xl-8 d-flex">
                <div class="my-auto">
                    <div class="benefits_text">
                        <p>{{getSetting()->choose}}</p>
                    </div>
                </div><!-- /.my-auto -->
            </div>
        </div>
        <div class="benefits_bottom_part">
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
                    <div class="benefits_single wow fadeInUp animated" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                        <div class="icon-box">
                            <i class="fa fa-star"></i>
                        </div>
                        <h3>Quality</h3>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
                    <div class="benefits_single wow fadeInUp animated" data-wow-delay="600ms" style="visibility: visible; animation-delay: 600ms; animation-name: fadeInUp;">
                        <div class="icon-box">
                           <i class="fa fa-check"></i> 
                       </div>
                       <h3>Reponsibility</h3>
                   </div>
               </div>
               <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <div class="benefits_single wow fadeInUp animated" data-wow-delay="900ms" style="visibility: visible; animation-delay: 900ms; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3>Teamwork</h3>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <div class="benefits_single wow fadeInUp animated" data-wow-delay="1200ms" style="visibility: visible; animation-delay: 1200ms; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <i class="fa fa-thumbs-up"></i>
                    </div>
                    <h3>Honest</h3>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <div class="benefits_single wow fadeInUp animated" data-wow-delay="900ms" style="visibility: visible; animation-delay: 900ms; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <i class="fa  fa-heart"></i>
                    </div>
                    <h3>Passion</h3>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <div class="benefits_single wow fadeInUp animated" data-wow-delay="1200ms" style="visibility: visible; animation-delay: 1200ms; animation-name: fadeInUp;">
                    <div class="icon-box">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    </div>
                    <h3>Attention</h3>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

@endsection