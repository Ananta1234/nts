@extends('front.layouts.default')
@section('content')
<section class="page-header" style="background-image: url(/frontend/assets/images/backgrounds/page-header-contact.jpg);">
	<div class="container">
		<h2>Product Details</h2>
		<ul class="thm-breadcrumb list-unstyled">
			<li><a href="index-2.html">Home</a></li>
			<li><span>{{$product->name}}</span></li>
		</ul>
	</div>
</section>
 <section class="product_detail">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="product_detail_image">
                            <img src="/images/products/{{$product->image}}" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="product_detail_content">
                            <h2>{{$product->name}}</h2>
                           
                            <div class="product_detail_text">
                                <p>{{$product->description}}</p>
                            </div>
                            <div class="product-quantity-box">
                                <div class="addto-cart-box">
                                    <button class="thm-btn" type="submit">Add to Cart</button>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection
