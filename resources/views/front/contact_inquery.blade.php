<!DOCTYPE html>
<html>
<head>
	<title>Customer Enquiry</title>
</head>
<body>
	<table>
		<tr><td>Dear Sir!</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Enquiry details are below: </td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Name: {{$name}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>phone: {{$phone}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Email: {{$email}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Subject: {{$subject}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Message: {{$comment}}</td></tr>
		<tr><td>&nbsp;</td></tr>

	</table>
</body>
</html>