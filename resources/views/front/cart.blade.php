@extends('front.layouts.default')
@section('content')
<section class="page-header" style="background-image: url(/frontend/assets/images/backgrounds/page-header-contact.jpg);">
	<div class="container">
		<h2>Cart</h2>
		<ul class="thm-breadcrumb list-unstyled">
			<li><a href="index-2.html">Home</a></li>
			<li><span>Cart</span></li>
		</ul>
	</div>
</section>
@include('admin.layouts.message')

   <section class="cart">
            <div class="container">
                @if(!$cartItem->isEmpty())
                <form method="post" action="{{route('front.order')}}">
                    @csrf
                    <div class="row">
                        <div class="col-xl-8 col-lg-8">
                            <div class="cart_table_box">
                             
                                <table class="cart_table">
                                    <thead class="cart_table_head">
                                        <tr>
                                            <th>Item</th>
                                            <th></th>
                                            <th>Quantity</th>
                                            <th style="text-align:center">Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($cartItem as $item)
                                            @php
                                                $product=\App\Models\Admin\Product::where('id',$item->product_id)->first(); 
                                            @endphp
                                        <tr>
                                            <td colspan="2">
                                                <div class="colum_box">
                                                    <div class="prod_thum">
                                                        <a href="#"><img src="{{asset('images/products/'.$product->image)}}" alt="" style="height:120px"></a>
                                                    </div>
                                                    <div class="title">
                                                        <h3 class="prod-title">{{$product->name}}</h3>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="pro_qty">
                                                <div class="product-quantity-box">
                                                    <div class="input-box">
                                                        <input type="number" name="qty[]" min="1" value="1" class="form-control">
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="text-align:center;">
                                                <div class="pro_remove">
                                                    <form method="post" action="{{route('front.removeCart')}}">
                                                        @csrf
                                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                                        <button type="submit"><i class="fas fa-times"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="order-form">
                                    <div class="form-group">
                                        <input type="text" name="full_name" class="order_control" placeholder="Enter your Full Name" required>
                                    </div>
                                     <div class="form-group">
                                        <input type="text" name="phone" class="order_control" placeholder="Enter your Phone Number" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="email" class="order_control" placeholder="Enter your Email" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="address" class="order_control" placeholder="Enter your Address" required>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="button_box">
                                
                                <button type="submit" class="thm-btn checkout_btn">Order Now</button>
                            </div>
                        </div>
                    </div>
                </form>
                 @else
                    <p style="color:#ddd;font-size:40px;text-align:center">Cart is Empty</p>
                @endif
            </div>
        </section>
@endsection