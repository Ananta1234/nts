<!DOCTYPE html>
<html>
<head>
	<title>Customer Order</title>
	<style type="text/css">
		table.border,.border tr th,.border tr td {
  border: 1px solid black;
  border-collapse: collapse;
}
	</style>
</head>
<body>
	<table>
		<tr><td>Dear Sir!</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Order details are below: </td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Name: {{$name}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>phone: {{$phone}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Email: {{$email}}</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>Address: {{$address}}</td></tr>

	</table>

	<table class="border">
		<thead>
			<tr>
				<th>Product Name</th>
				<th>Quantity</th>
			</tr>
		</thead>
		<tbody>
			@foreach($product_id as $key=>$productId)
			<tr>

					<td>{{\App\Models\Admin\Product::where('id',$productId->product_id)->first()->name}}</td>
					<td>{{$qty[$key]}}</td>
			</tr>
			@endforeach

		</tbody>
	</table>
</body>
</html>