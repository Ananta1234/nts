@extends('front.layouts.default')
@section('content')
<section class="page-header" style="background-image: url(/frontend/assets/images/backgrounds/page-header-contact.jpg);">
	<div class="container">
		<h2>Gallery</h2>
		<ul class="thm-breadcrumb list-unstyled">
			<li><a href="index-2.html">Home</a></li>
			<li><span>Gallery</span></li>
		</ul>
	</div>
</section>
   <section class="gallery_two">
            <div class="container">
                <div class="row masonary-layout">
                	@foreach($data['galleries'] as $gallery)
                    <div class="col-xl-4 col-lg-6 col-md-6 masonary-item">
                        <div class="gallery_two_single">
                            <div class="gallery_two_image">
                                <img src="/images/gallery/{{$gallery->image}}" alt="">
                                <div class="gallery_two_hover_box">
                                    <div class="gallery_two_icon">
                                        <a class="img-popup" href="/images/gallery/{{$gallery->image}}"><span
                                                class="icon-plus-symbol"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
@endsection