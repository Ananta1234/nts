@extends('front.layouts.default')
@section('content')
<section class="page-header" style="background-image: url(/frontend/assets/images/backgrounds/page-header-contact.jpg);">
	<div class="container">
		<h2>{{$category->name}}</h2>
		<ul class="thm-breadcrumb list-unstyled">
			<li><a href="index-2.html">Home</a></li>
			<li><span>{{$category->name}}</span></li>
		</ul>
	</div>
</section>


<section class="categoryproduct" style="padding:100px 0">
    <div class="container">
        <div class="row">
            @forelse($category->frontProducts() as $product)
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6 list-item">
                <div class="recent_project_single wow fadeInUp animated" data-wow-delay="300ms" style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                    <div class="project_img_box">
                        <img src="{{asset('images/products/'.$product->image)}}">

                    </div>
                    <div class="blog-one__content">

                        <h3><a href="{{route('front.productdetails',$product->slug)}}">{{$product->name}}</a></h3>

                        <div class="read_more_btn">
                           <form action="{{route('front.cartStore')}}" method="post">
                            @csrf
                            <input type="hidden" name="product_id" value="{{$product->id}}">
                            <button type="submit"><i class="fa fa-shopping-cart"></i>Order Now</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <p style="text-align:center;font-size:24px;color:#ddd;width:100%">No product available</p>
        @endforelse
    </div>
    @if($category->frontProducts()->isEmpty())
    @else
    <div id="pagination-container"></div>
    @endif
</div>
</section>

@endsection

@push('frontscripts')
<script>
  let number=12;
</script>
@include('admin.scripts.pagination')
@endpush