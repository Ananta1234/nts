@extends('admin.layouts.default')
@section('header','Banner')
@section('subheader','Banner List')
@section('content')
@include('admin.layouts.message')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Banner List</h3>
    <div class="action-button float-right">
    	<a href="{{route('banner.create')}}" class="btn btn-success">Create Banner</a>
    </div>
   
  </div>
  <!-- /.card-header -->
   <div class="product-block">
   	
    	<div class="row row-mb-30">
    		@include('admin.banner.bannerItem')
    	</div>
    	<div id="pagination-container"></div>
    </div>

  
</div>
@endsection
@push('scripts')
<script>
	let number=1;
</script>
@include('admin.scripts.pagination')
@endpush