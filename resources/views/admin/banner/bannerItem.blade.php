@forelse($banners as $banner)
<div class="col-md-12 list-item">
 <div class="product-image">
    <img src="{{asset('images/banners/'.$banner->image)}}">
</div>
<div class="product-details">
    <div class="p-name">
       <h3>{{$banner->title}}</h3>
       <p>{{$banner->description}}</p>
   </div>
   <div class="action-product">
       <div class="more-details flex">
          <p>{{$banner->created_at->diffForHumans()}}</p>
      </div>
      <div class="edit-action">
          <a href="{{route('banner.show',$banner->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
          
      </div>
      <div class="delete-action">
          <a href="" class="btn btn-danger"><i class="fa fa-trash"></i></a>
      </div>
  </div>
</div>
</div>
@empty
<p class="empty">No banner available</p>
@endforelse