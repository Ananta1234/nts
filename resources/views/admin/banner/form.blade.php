@extends('admin.layouts.default')
@section('header','Banner')
@if(isset($banner))
	@section('subheader','Edit Banner')
@else
	@section('subheader','Create Banner')
@endif
@section('content')
@include('admin.layouts.message')
<div class="card card-primary">
	<div class="card-header">
		<h3 class="card-title">{{isset($banner) ? 'Edit Banner':'Create Banner'}}</h3>
	</div>
	<!-- /.card-header -->
	<!-- form start -->
	@if(isset($banner))
	<form role="form" action="{{route('banner.update',$banner->id)}}" method="post"  enctype='multipart/form-data'>
		@method('PATCH')
		@else
		<form role="form" action="{{route('banner.store')}}" method="post" enctype='multipart/form-data'>
			@endif
			@csrf
			<div class="card-body">
				<div  class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label for="name">Title <span class="red">*</span></label>
							<input type="text" class="form-control" id="name" placeholder="Enter Title" name="title" value="{{isset($banner) ? $banner->title : old('name')}}" required>
							@error('title')
							<span class="red">{{$message}}</span>
							@enderror
						</div>
						<div class='form-group'>
							<label for="image">Image</label>@if(isset($banner)) 
								<img src="{{asset('images/banners/'.$banner->image)}}" style="width:50px">
							 @endif
							<input type="file" name="image" class="form-control">
							@error('image')
								<span class="red">{{$message}}</span>
							@enderror
						</div>
						<div class="form-group">
							<label for="name">Description</label>
							<textarea class="form-control" name="description" rows="6" placeholder="Description">{{isset($banner) ? $banner->description : old('description')}}</textarea>
						</div>

						<div class="form-check">
							<input type="checkbox" name="status" class="form-check-input" id="status" value="1" @if(isset($banner)) @if($banner->status==1) checked @endif  @endif>
							<label class="form-check-label" for="staus">Status</label>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->

			<div class="card-footer">
				<button type="submit" class="btn btn-success">{{isset($banner) ? 'Update' : 'Create'}}</button>
			</div>
		</form>
	</div>

	@endsection