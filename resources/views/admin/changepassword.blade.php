@extends('admin.layouts.default')
@section('header','Setting')
@section('subheader','Change Passowrd')
@section('content')
@include('admin.layouts.message')
<div class="card card-primary">
	<div class="card-header">
		<h3 class="card-title">Change Passowrd</h3>


	</div>
	<!-- /.card-header -->
	<form method="post" action="{{route('admin.changepassword')}}">
		
		<div class="card-body">
			<div  class="row">
				<div class="col-md-8">
					@csrf
					<div class="form-group">
						<label class="control-label">Email Address</label>
						<input type="text" value="{{auth()->user()->email}}" class="form-control" readonly autocomplete="off">
					</div>
					<div class="form-group">
						<label class="control-label">Current Password</label>
						@error('currentpassword')
						<small class="block red">{{$message}}</small>
						@enderror
						<input type="password" class="form-control" name="currentpassword" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label class="control-label">New Password</label>
						@error('newpassword')
						<small class="block red">{{$message}}</small>
						@enderror
						<input type="password" class="form-control" name="newpassword" autocomplete="off" required value="{{old('newpassword')}}">
					</div>
					<div class="form-group">
						<label class="control-label">Confirm Password</label>
						@error('confirmpassword')
						<small class="block red">{{$message}}</small>
						@enderror
						<input type="password" class="form-control" name="confirmpassword" autocomplete="off" required>
					</div>
				</div>
			</div>
		</div>
		<!-- /.card-body -->

		<div class="card-footer">
			<button type="submit" class="btn btn-success">Change</button>
		</div>
	</form>
</div>
@endsection
@push('scripts')

@endpush