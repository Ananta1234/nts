@extends('admin.layouts.default')
@section('header','Setting')
@section('subheader','General Setting')
@section('content')
@include('admin.layouts.message')
<div class="mgs-before"></div>
<div class="card card-primary">
	<form method="post" action="{{route('setting.store')}}" enctype='multipart/form-data'>
		@csrf
		<div class="card-header">
			<h3 class="card-title">General Setting</h3>
			<div class="action-button float-right">
				<button type="submit" class="float-right btn btn-primary">Save</button>

			</div>

		</div>
		<!-- /.card-header -->
		<div class="product-block">

			<div class="row row-mb-30">


				<div class="col-lg-5 col-md-5 col-sm-12">
					<div class="form-group">
						<label class="control-label">Logo <img style="width:50px;height:30px" src="{{asset('images/logo/'.@$setting->logo)}}"></label>
						@error('image')
						<small class="red block">{{$message}}</small>
						@enderror
						<input type="file" name="image" class="form-control">
					</div>
					<div class="form-group">
						<label class="control-label">Email</label>
						@error('email')
						<small class="red block">{{$message}}</small>
						@enderror
						<input type="text" name="email" class="form-control" placeholder="Email Address" value="{{@$setting->email ?? old('email')}}">
					</div>
					<div class="form-group">
						<label class="control-label">Phone Number</label>
						@error('phone')
						<small class="red block">{{$message}}</small>
						@enderror
						<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{@$setting->phone ?? old('phone')}}">
					</div>
					<div class="form-group">
						<label class="control-label">Address</label>
						@error('address')
						<small class="red block">{{$message}}</small>
						@enderror
						<input type="text" name="address" class="form-control" placeholder="Address" value="{{@$setting->address ?? old('address')}}">
					</div>
					<div  class="form-group">
						<label class="control-label">Social Links</label>
						@error('social')
						<small class="red block">{{$message}}</small>
						@enderror
						<small class="eg">For eg:https://www.youtube.com</small>
						<div class="field_wrapper">

							<div class="social-links">
								@foreach(@$setting['link'] as $links)
								<div class="socialData m-b-7">
									<input  class="link-control" type="text" name="social" value="{{$links->social}}" readonly/>
									<a href="javascript:void(0);" class="removeSocial btn btn-danger btn-xs" data-id='{{$links->id}}'><i class="fa fa-times"></i></a>
								</div>
								@endforeach
							</div>
							<div class="m-b-7">
								<input  class="link-control" type="url" name="social[]" value=""/>
								<a href="javascript:void(0);" class="add_button tn btn-success btn-xs" title="Add field"><i class="fa fa-plus"></i></a>
							</div>
						</div>

					</div>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-12">
					<div class="form-group">
						<label class="control-label">About Us</label>
						@error('about')
						<small class="red block">{{$message}}</small>
						@enderror
						<textarea name="about" class="form-control"  id="editor" rows="20">{{@$setting->about ?? old('about')}}</textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Why Choose Us</label>
						@error('choose')
						<small class="red block">{{$message}}</small>
						@enderror
						<textarea name="choose" class="form-control"  id="editor" rows="5">{{@$setting->choose ?? old('choose')}}</textarea>
					</div>
				</div>
				
			</div>

		</div>
	</form>

</div>
@endsection
@push('scripts')
@include('admin.scripts.editor')
<script type="text/javascript">
	var addButton = $('.add_button');
	var wrapper = $('.field_wrapper');
	var fieldHTML = '<div class="m-b-7"><input class="link-control" type="url" name="social[]" value=""/> <a href="javascript:void(0);" class="btn btn-danger btn-xs remove_button"><i class="fa fa-minus"></i></a></div>';
	$(addButton).click(function(){

		$(wrapper).append(fieldHTML);

	});
	$(wrapper).on('click', '.remove_button', function(e){
		e.preventDefault();
		$(this).parent('div').remove();
	});
	$(document).on('click','.removeSocial',function(e){
		e.preventDefault();
		let id=$(this).attr('data-id');
		removeSocialLink(id);
		$(this).parent('.socialData').remove();
	});

	function removeSocialLink(id)
	{
		$.ajax({
			url:"{{route('removeLinks')}}",
			type:"post",
			data:{id:id,_token:"{{csrf_token()}}"},
			dataType:"json",
			success:function(data)
			{
				const message="<div class='alert alert-danger action-response' role='alert'>\
					  <button type='button' class='close' data-dismiss='alert' aria-label='Cose''><span aria-hidden=''true''>&times;</span></button>\
					  <strong>Success!</strong> Deleted Successfully\
					</div>";
					$(message).insertBefore('.mgs-before');
					window.setTimeout(function() {
				    $(".action-response").fadeTo(500, 0).slideUp(500, function(){
				        $(this).remove(); 
				    });
				}, 2000);

			}
		});
	}
</script>
@endpush