@extends('admin.layouts.default')
@section('header','Category')
@section('subheader','Category List')
@section('content')
@include('admin.layouts.message')
<div class="row mgs-before">
	<div class="col-md-5">
           
     @include('admin.category.form')

    </div>
	<div class="col-lg-7 col-md-7 col-sm-12">
		
		@include('admin.category.categoryList')
	</div>
</div>
@endsection
@push('scripts')
@include('admin.scripts.sorting');
@endpush