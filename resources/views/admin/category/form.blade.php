<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">{{isset($category) ? 'Edit Category':'Create Category'}}</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
    @if(isset($category))
      <form role="form" action="{{route('category.update',$category->slug)}}" method="post">
        @method('PATCH')
    @else
      <form role="form" action="{{route('category.store')}}" method="post">
    @endif
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="name">Name <span class="red">*</span></label>
        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="{{isset($category) ? $category->name : old('name')}}" required>
        @error('name')
          <span class="red">{{$message}}</span>
        @enderror
      </div>

      <div class="form-group">
        <label for="name">Description</label>
        <textarea class="form-control" name="description" rows="" placeholder="Description">{{isset($category) ? $category->description : old('description')}}</textarea>
      </div>
     
      <div class="form-check">
        <input type="checkbox" name="status" class="form-check-input" id="status" value="1" @if(isset($category)) @if($category->status==1) checked @endif  @endif>
        <label class="form-check-label" for="staus">Status</label>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-success">{{isset($category) ? 'Update' : 'Create'}}</button>
    </div>
  </form>
</div>
