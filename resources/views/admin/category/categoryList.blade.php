<div  id="categoryList" class="categoryList">
	<ul class="sort_category list-group">
		@foreach(getOrderCategory() as $category)
		<li class="list-group-item" data-id="{{$category->id}}"><span class="handle" title="Drag & Drop"></span>&nbsp;&nbsp;{{$category->name}}&nbsp;&nbsp;<small>{{$category->created_at->diffForHumans()}}</small>
			<div class="category-action-button">
				<a href="{{route('category.destroy',$category->slug)}}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
				<a href="{{route('category.update',$category->slug)}}" id="categoryEdit" class="white btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
			</div>
		</li>
		@endforeach
	</ul>
</div>