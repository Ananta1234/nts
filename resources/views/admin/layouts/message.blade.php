@if(session()->has('success'))
<div class="alert alert-success action-response" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {!! session()->get('success') !!}
</div>
@endif

@if(session()->has('danger'))
<div class="alert alert-danger action-response" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 {!! session()->get('danger') !!}
</div>
@endif