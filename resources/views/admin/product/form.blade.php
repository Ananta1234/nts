@extends('admin.layouts.default')
@section('header','Product')
@if(isset($product))
	@section('subheader','Edit Product')
@else
	@section('subheader','Create Product')
@endif
@section('content')
@include('admin.layouts.message')
<div class="card card-primary">
	<div class="card-header">
		<h3 class="card-title">{{isset($product) ? 'Edit Product':'Create Product'}}</h3>
	</div>
	<!-- /.card-header -->
	<!-- form start -->
	@if(isset($product))
	<form role="form" action="{{route('product.update',$product->slug)}}" method="post"  enctype='multipart/form-data'>
		@method('PATCH')
		@else
		<form role="form" action="{{route('product.store')}}" method="post" enctype='multipart/form-data'>
			@endif
			@csrf
			<div class="card-body">
				<div  class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label for="name">Name <span class="red">*</span></label>
							<input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="{{isset($product) ? $product->name : old('name')}}" required>
							@error('name')
							<span class="red">{{$message}}</span>
							@enderror
						</div>
						<div class='form-group'>
							<label for="image">Image</label>@if(isset($product)) 
								<img src="{{asset('images/products/'.$product->image)}}" style="width:50px">
							 @endif
							<input type="file" name="image" class="form-control">
							@error('image')
								<span class="red">{{$message}}</span>
							@enderror
						</div>
						<div class="form-group">
							<label for="name">Description</label>
							<textarea class="form-control" name="description" rows="6" placeholder="Description">{{isset($product) ? $product->description : old('description')}}</textarea>
						</div>

						<div class="form-check">
							<input type="checkbox" name="status" class="form-check-input" id="status" value="1" @if(isset($product)) @if($product->status==1) checked @endif  @endif>
							<label class="form-check-label" for="staus">Status</label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="productcategory">
							<h3>Under Category</h3>
							@error('category_id')
								<span class="red">{{$message}}</span>
							@enderror
							<ul>
								@foreach(getOrderCategory() as $category)
									<li><input type="radio" name="category_id" value="{{$category->id}}" @if(isset($product)) @if($category->id==$product->category_id) checked @endif @endif>&nbsp;&nbsp;&nbsp;&nbsp;{{$category->name}}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->

			<div class="card-footer">
				<button type="submit" class="btn btn-success">{{isset($product) ? 'Update' : 'Create'}}</button>
			</div>
		</form>
	</div>

	@endsection