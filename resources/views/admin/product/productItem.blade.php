@forelse($products as $product)
<div class="col-md-3 list-item">
 <div class="product-image">
    <img src="{{asset('images/products/'.$product->image)}}">
</div>
<div class="product-details">
    <div class="p-name">
       <h3>{{$product->name}}</h3>
   </div>
   <div class="action-product">
       <div class="more-details flex">
          <button class="btn btn-default">More Details</button>
      </div>
      <div class="edit-action">
          <a href="{{route('product.show',$product->slug)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
          
      </div>
      <div class="delete-action">
          <a href="{{route('product.destroy',$product->slug)}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
      </div>
  </div>
</div>
</div>
@empty
<p class="empty">No product available</p>
@endforelse