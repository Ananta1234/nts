@extends('admin.layouts.default')
@section('header','Product')
@section('subheader','Product List')
@section('content')
@include('admin.layouts.message')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Product List</h3>
    <div class="action-button float-right">
    	<a href="{{route('product.create')}}" class="btn btn-success">Create Product</a>
    </div>
   
  </div>
  <!-- /.card-header -->
   <div class="product-block">
   	<div class="dropdown">
   		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter By Category
   			<span class="caret"></span></button>
   			<ul class="dropdown-menu">
   				@foreach(getOrderCategory() as $category)
   				<li><a href="{{route('product.index',$category->slug)}}">{{$category->name}}</a></li>
   				@endforeach

   			</ul>
   		</div> 
    	<div class="row row-mb-30">
    		  @include('admin.product.productItem')
    	</div>
    	<div id="pagination-container"></div>
    </div>

  
</div>
@endsection
@push('scripts')
<script>
    let number=12;
</script>
@include('admin.scripts.pagination')
@endpush