<script type="text/javascript">
	function sortingApi(ids)
	{
		$.ajaxSetup({headers:{'x-CSRF-TOKEN': '{{csrf_token()}}'}});
		$.ajax({
			url:"{{route('categorySort')}}",
			type:"POST",
			data:{
				ids:ids,
			},
			success:function(data)
			{
				const message="<div class='alert alert-success action-response' role='alert'>\
					  <button type='button' class='close' data-dismiss='alert' aria-label='Cose''><span aria-hidden=''true''>&times;</span></button>\
					  <strong>Success!</strong> Sorting Successfully\
					</div>";
					$(message).insertBefore('.mgs-before');
					window.setTimeout(function() {
				    $(".action-response").fadeTo(500, 0).slideUp(500, function(){
				        $(this).remove(); 
				    });
				}, 2000);

			}
		});
	}
	let target=$('.sort_category');
	target.sortable({
		handle:'.handle',
		placeholder:'highlight',
		axis:'y',
		update:function(e,ui)
		{
			let sortData=target.sortable('toArray',{attribute:'data-id'})
			sortingApi(sortData.join(','))
		}
	});
</script>
