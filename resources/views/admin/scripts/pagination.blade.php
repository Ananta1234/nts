<script src="{{asset('admin/js/jquery.simplePagination.js')}}"></script>
<script>
    let items = $(".list-item");
    let numItems = items.length;
    let perPage = number;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            let showFrom = perPage * (pageNumber - 1);
            let showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
</script>