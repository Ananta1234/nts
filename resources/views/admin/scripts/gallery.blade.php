<script type="text/javascript">
	$(document).ready(function(){
		$.ajaxSetup({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$(document).on("click", ".browse", function() {
			var file = $(this).parents().find(".galleryImages");
			file.trigger("click");
		});
		$('#browseFile').on('change',function(evt){
			if (window.File && window.FileReader && window.FileList && window.Blob)
			{
				let files = $(this)[0];
				let file_upload=new FormData();

				$.each(files.files,function(index,file){
					file_upload.append('files[]',files.files[index]);	
				});
				fileApi(file_upload);
				const url="{{route('removeImage')}}";
				const list='#list';
				deletePreview(list,url)
			}
		});
		function fileApi(file_upload)
		{
			$.ajax({
				url:"{{route('uploadFile')}}",
				type:'post',
				data:file_upload,
				processData: false,
				contentType: false,
				success:function(data)
				{
					console.log(data);
					$.each(data,function(index,file){
						var span = document.createElement('span');
						span.classList.add('preview_block');
						span.classList.add('col-md-3');
						span.innerHTML = ['<img class="thumb" src="/temp/',file,'" alt="',file,'"/><span class="remove_img_preview" id="',file,'"></span>'].join('');
						document.getElementById('list').insertBefore(span, null);
						

					});
					if($('#list .preview_block').length>0)
						$('<button class="btn btn-success uploads" type"submit>Upload</button>').insertAfter('.browse');
					
				}
			});
		}
		function deletePreview(list,url)
		{
			$(list).on('click','.remove_img_preview',function(){
				var name=$(this).attr('id');
				$(this).parent('span').remove();						$.ajax({
					url:url,
					type:'post',
					data:{name:name},
					success:function(data)
					{
						if($('#list .preview_block').length<=0)
							$('.uploads').remove();
						console.log(data);
					}
				});
			});
		}
	});
</script>