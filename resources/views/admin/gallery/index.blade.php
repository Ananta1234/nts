@extends('admin.layouts.default')
@section('header','Gallery')
@section('subheader','Gallery List')
@section('content')
@include('admin.layouts.message')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Galley List</h3>


  </div>
  <!-- /.card-header -->
  <div class="product-block">
     <div id="list" class="row"></div>
   <div class="row row-mb-30">
    <div class="ml-2 col-sm-12">
      <form method="post" id="image-form" action="{{route('gallery.store')}}" enctype='multipart/form-data'>
        @csrf
        @error('files')
          <span class="red">{{$message}}</span>
        @enderror
        <input type="file" name="files[]" id="browseFile" class="galleryImages" accept="image/*" multiple>
        <div class="input-group my-3">
          <input type="text" class="form-control" disabled placeholder="Upload Image" id="file">
          <div class="input-group-append">
            <button type="button" class="browse btn btn-primary">Browse...</button>
          </div>
        </div>
      </form>
    </div>


  </div>
  <form method="post" action="{{route('gallery.destroy')}}">
    @csrf
    <button type="submit" class="btn btn-danger">Delete</button>
    <div class="row row-mb-30">
      @include('admin.gallery.galleryItem')
    </div>
 </form>
  <div id="pagination-container"></div>
</div>


</div>
@endsection
@push('scripts')
<script>
  let number=12;
</script>
@include('admin.scripts.pagination')
@include('admin.scripts.gallery')
@endpush