(function ($) {
    "use strict";
    $('.category-tab-panel li').on('click',function(){
         $('.category-tab-panel').find(".active").removeClass('active');
         $(this).addClass('active')
    });

    //Main Slider / Banner Carousel
    if ($(".banner-carousel").length) {
        $(".banner-carousel").owlCarousel({
            loop: true,
            animateOut: "fadeOut",
            animateIn: "fadeIn",
            margin: 0,
            nav: true,
            dots: true,
            smartSpeed: 500,
            autoplay: true,
            autoplayTimeout: 7000,
            navText: [
                '<span class="icon fa fa-angle-left"></span><p>Prev</p>',
                '<p>Next</p><span class="icon fa fa-angle-right"></span>',
            ],
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                800: {
                    items: 1,
                },
                992: {
                    items: 1,
                },
            },
        });
    }
  

    //Submenu Dropdown Toggle
    if ($(".main-nav__main-navigation li.dropdown ul").length) {
        $(".main-nav__main-navigation li.dropdown")
            .children("a")
            .append(
                '<button class="dropdown-btn"><i class="fa fa-angle-right"></i></button>'
            );
    }

    // mobile menu
    if ($(".main-nav__main-navigation").length) {
        let mobileNavContainer = $(".mobile-nav__container");
        let mainNavContent = $(".main-nav__main-navigation").html();

        mobileNavContainer.append(mainNavContent);

        //Dropdown Button
        mobileNavContainer
            .find("li.dropdown .dropdown-btn")
            .on("click", function (e) {
                $(this).toggleClass("open");
                $(this).parent().parent().children("ul").slideToggle(500);
                e.preventDefault();
            });
    }

    if ($(".stricky").length) {
        $(".stricky")
            .addClass("original")
            .clone(true)
            .insertAfter(".stricky")
            .addClass("stricked-menu")
            .removeClass("original");
    }

    if ($(".side-menu__toggler").length) {
        $(".side-menu__toggler").on("click", function (e) {
            $(".side-menu__block").toggleClass("active");
            e.preventDefault();
        });
    }

    if ($(".side-menu__block-overlay").length) {
        $(".side-menu__block-overlay").on("click", function (e) {
            $(".side-menu__block").removeClass("active");
            e.preventDefault();
        });
    }

    if ($(".scroll-to-target").length) {
        $(".scroll-to-target").on("click", function () {
            var target = $(this).attr("data-target");
            // animate
            $("html, body").animate(
                {
                    scrollTop: $(target).offset().top,
                },
                1000
            );

            return false;
        });
    }


    if ($(".wow").length) {
        var wow = new WOW({
            boxClass: "wow", // animated element css class (default is wow)
            animateClass: "animated", // animation css class (default is animated)
            offset: 250, // distance to the element when triggering the animation (default is 0)
            mobile: true, // trigger animations on mobile devices (default is true)
            live: true, // act on asynchronously loaded content (default is true)
        });
        wow.init();
    }

    function SmoothMenuScroll() {
        var anchor = $(".scrollToLink");
        if (anchor.length) {
            anchor.children("a").bind("click", function (event) {
                if ($(window).scrollTop() > 10) {
                    var headerH = "67";
                } else {
                    var headerH = "100";
                }
                var target = $(this);
                $("html, body")
                    .stop()
                    .animate(
                        {
                            scrollTop:
                                $(target.attr("href")).offset().top -
                                headerH +
                                "px",
                        },
                        1200,
                        "easeInOutExpo"
                    );
                anchor.removeClass("current");
                target.parent().addClass("current");
                event.preventDefault();
            });
        }
    }
    SmoothMenuScroll();

    function OnePageMenuScroll() {
        var windscroll = $(window).scrollTop();
        if (windscroll >= 100) {
            var menuAnchor = $(".one-page-scroll-menu .scrollToLink").children(
                "a"
            );
            menuAnchor.each(function () {
                // grabing section id dynamically
                var sections = $(this).attr("href");
                $(sections).each(function () {
                    // checking is scroll bar are in section
                    if ($(this).offset().top <= windscroll + 100) {
                        // grabing the dynamic id of section
                        var Sectionid = $(sections).attr("id");
                        // removing current class from others
                        $(".one-page-scroll-menu")
                            .find("li")
                            .removeClass("current");
                        // adding current class to related navigation
                        $(".one-page-scroll-menu")
                            .find("a[href*=\\#" + Sectionid + "]")
                            .parent()
                            .addClass("current");
                    }
                });
            });
        } else {
            $(".one-page-scroll-menu li.current").removeClass("current");
            $(".one-page-scroll-menu li:first").addClass("current");
        }
    }

  
   

  
  

    $(window).on("scroll", function () {
        if ($(".stricked-menu").length) {
            var headerScrollPos = 100;
            var stricky = $(".stricked-menu");
            if ($(window).scrollTop() > headerScrollPos) {
                stricky.addClass("stricky-fixed");
            } else if ($(this).scrollTop() <= headerScrollPos) {
                stricky.removeClass("stricky-fixed");
            }
        }
        OnePageMenuScroll();
        if ($(".scroll-to-top").length) {
            var strickyScrollPos = 100;
            if ($(window).scrollTop() > strickyScrollPos) {
                $(".scroll-to-top").fadeIn(500);
            } else if ($(this).scrollTop() <= strickyScrollPos) {
                $(".scroll-to-top").fadeOut(500);
            }
        }
    });

    $(window).on("load", function () {
        if ($(".tour-details__gallery-thumb-carousel").length) {
            var testimonialsTwoThumbCarousel = new Swiper(
                ".tour-details__gallery-thumb-carousel",
                {
                    slidesPerView: 5,
                    spaceBetween: 10,
                    mousewheel: true,
                    speed: 1400,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    loop: true,
                    autoplay: {
                        delay: 5000,
                    },
                }
            );
        }

      

       

        if ($(".side-menu__block-inner").length) {
            $(".side-menu__block-inner").mCustomScrollbar({
                axis: "y",
                theme: "dark",
            });
        }

       

        if ($(".preloader").length) {
            $(".preloader").fadeOut();
        }



    });
     if ($(".masonary-layout").length) {
            $(".masonary-layout").isotope({
                layoutMode: "masonry",
                itemSelector: ".masonary-item",
            });
    }
    if ($(".img-popup").length) {
        var groups = {};
        $(".img-popup").each(function () {
            var id = parseInt($(this).attr("data-group"), 10);

            if (!groups[id]) {
                groups[id] = [];
            }

            groups[id].push(this);
        });

        $.each(groups, function () {
            $(this).magnificPopup({
                type: "image",
                closeOnContentClick: true,
                closeBtnInside: false,
                gallery: {
                    enabled: true,
                },
            });
        });
    }
   /* $('.read-button').click(function(e) {
       
        let iconClass=".read-button i";
      $('.moretext').slideToggle();
              if ($(iconClass).attr('class') == "fa fa-arrow-down") {
                
                $(iconClass).removeClass("fa fa-arrow-down");
                $(iconClass).addClass("fa fa-arrow-up");

            } 
            else {
                $(iconClass).removeClass("fa fa-arrow-up");
                $(iconClass).addClass("fa fa-arrow-down");
            }
    });
   */
  
})(jQuery);
