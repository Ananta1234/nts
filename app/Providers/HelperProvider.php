<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadHelpers();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    protected function loadHelpers()
    {
        foreach (glob(app_path() . '/Http/Helpers/*.php') as $file)
        {
            require_once($file);
        }
    }
}
