<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $guarded=[];



    public function storeCart($data)
    {
    	self::create($data);
    }

    public function getSessionCart($sessionId)
    {
    	return self::where('session_id',$sessionId)->get();
    }
}
