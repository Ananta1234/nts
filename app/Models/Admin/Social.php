<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function getSocial()
    {
    	return self::get();
    }

    public function deleteSocial($socialId)
    {
    	self::where('id',$socialId)->delete();
    }
}
