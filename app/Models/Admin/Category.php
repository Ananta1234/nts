<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Product;
class Category extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function frontProducts()
    {
        return $this->products()->where('status',1)->get();
    }
    public function totalData()
    {
    	return $this->count();
    }
    public function order()
    {
    	return $this->totalData()+1;
    }
    
    public function storeData($request)
    {
    	self::create($request);
    }
}
