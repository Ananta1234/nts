<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function getLatestGallery()
    {
    	return self::latest()->get();
    }

    public function storeData($data)
    {
    	self::create($data);
    }

    public function deleteData($galleryId)
    {
    	self::whereIn('id',$galleryId)->delete();
    }
}
