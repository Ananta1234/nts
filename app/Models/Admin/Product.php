<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Category;

class Product extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function getLatest()
    {
    	return self::latest()->get();
    }

    public function storeData($data)
    {
        self::create($data);
    }
   
}
