<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function getLatestBanner()
    {
    	return self::latest()->get();
    }

    public function storeData($data)
    {
    	self::create($data);
    }
}
