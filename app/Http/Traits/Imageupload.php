<?php

namespace App\Http\Traits;
use Image;
trait Imageupload
{
	public function imageUpload($image,$path,$width,$height)
	{
		$filename=$image->getClientOriginalName();
    	$uniqueName=rand().$filename;
    	$path=$path.'/'.$uniqueName;
    	$imageFile=\Image::make($image);
    	$imageFile->resize($width,$height, function ($constraint) {
			$constraint->aspectRatio();
			    $constraint->upsize();
		})->save($path);
		return $uniqueName;
	}



	public function deleteImage($image,$path)
	{
		$path=$path.'/'.$image;
		\File::delete($path);
	}

	protected function requestImage($imageName=null)
	{
		
			$path=$this->path();
			$width=$this->width();
			$height=$this->height();
			$image=request()->image;
			$attributes['image']=$this->imageUpload($image,$path,$width,$height);
			if(!empty($imageName))
				$this->deleteImage($imageName,$path);
			return $attributes['image'];
	}


}