<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Setting;
use App\Models\Admin\Social;
use App\Http\Traits\Imageupload;


class SettingController extends Controller
{
	use Imageupload;
    public function __construct()
    {
    	$this->setting=new Setting();

    }

    public function index()
    {
    	$setting=getSetting();
		$setting['link']=(new Social)->getSocial();
    	return view('admin.setting.index',compact('setting'));
    }

    public function store(Request $request)
	{
		$attributes=$this->validate($request,[
			'email'=>'required',
			'phone'=>'required',
			'address'=>'required',
			'about'=>"required",
			'image'=>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		if($request->social)
			$this->requestSocial($request->social);
		$data=$request->except(['image','social']);
		if($request->hasFile('image'))
		{
			$data['logo']=$this->requestImage();
			$logo=$this->setting->pluck('logo')->first();
			if(!empty($logo))
				$this->deleteImage($logo,$this->path());
		}
		$this->setting->updateOrCreate(['id' => 1],$data);
		return back()->with('success','Saved Successfully');
	}

	public function deleteSocial(Request $request)
	{
		(new Social)->deleteSocial($request->id);
		return response()->json(['success'=>'Social links deleted successfully']);
	}

	protected function Path()
	{
		return 'images/logo';
	}

    protected function width()
    {
    	return 596;
    }

    protected function height()
    {
    	return 288;
    }

    protected function requestSocial($request)
    {
    	foreach($request as $link)
		{
			if(!empty($link))
				(new Social)->create(['social'=>$link]);
		}
    }
}
