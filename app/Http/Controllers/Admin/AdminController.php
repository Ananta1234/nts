<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
    	return view('admin.index');
    }


    public function changePass()
	{
		return view('admin.changepassword');
	}
	public  function change(Request $request)
	{
		$this->validate($request,[
			'currentpassword'=>'required',
			'newpassword'=>'required',
			'confirmpassword'=>"required|same:newpassword"
		]);
		$user=User::where(['id'=>Auth::user()->id])->first();
		$currentpassword=$request->currentpassword;
		if(Hash::check($currentpassword,$user->password))
        {
            $newpassword=bcrypt($request->newpassword);
            User::where('id',Auth::user()->id)->update(['password'=>$newpassword]);
            return back()->with('success','Password Changed Successfuly');
        }
        return back()->with('danger','Incorrect Password');
	}
}
