<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Admin\Gallery;
use File;
use App\Http\Traits\Imageupload;

class GalleryController extends Controller
{
	use Imageupload;
	public function __construct()
	{
		$this->gallery=new Gallery();
	}
    public function index()
    {
      $galleries=$this->gallery->getLatestGallery();
      if (file_exists($this->tempPath()))
        $this->deleteTempFolder();
    	return view('admin.gallery.index',compact('galleries'));
    }

    public function store(Request $request)
    {
        $request->validate([
        'files'=>'nullable',
        'files.*' => 'mimes:png,jpg,jpeg,svg|max:2048',
        ]);
        $filename=$this->findFile();
        foreach($filename as $file)
        {
          $tempPath=$this->tempPath().'/'.$file;
          $originalPath=$this->path().'/'.$file;
          File::move($tempPath,$originalPath);
          $data['image']=$file;
          $this->gallery->storeData($data);
        }
        $this->deleteTempFolder();
        return back()->with('success','Uploaded Successsfully');
    }
    
    public function destroy(Request $request)
    {
      if(empty($request->gallery))
        return back()->with('danger','Please select at least on to delete');
      $path=$this->path();
      foreach($request->gallery as $galleryId)
      {
        $image=$this->gallery->where('id',$galleryId)->first()->image;
        $this->deleteImage($image,$path);

      }
     
      $this->gallery->deleteData($request->gallery);
      return back()->with('danger','Delete Successfully');
    }      

    public function uploadImage(Request $request)
  	{
  		$path = $this->tempPath();
  		if(!File::isDirectory($path))
  		    File::makeDirectory($path, 0777, true, true);
    		foreach($request->file('files') as $file)
    		{
    			$this->imageUpload($file,$path,500,350);
    		}
    		$namestore=$this->findFile();
    		return response()->json($namestore);
    		
  	}

    public function removePreview(Request $request)
    {
      	$path=$this->tempPath();
  		  \File::delete($path.'/'.$request->name);
		    return 'success';
    }
    public function deleteTempFolder()
    {
        \File::deleteDirectory($this->tempPath());
    }

    protected function path()
    {
    	return 'images/gallery';
    }
    protected function tempPath()
    {
    	return public_path('temp');
    }
    protected function findFile()
  	{
  		$namestore=array();
  		foreach (glob(public_path() . '/temp/*.*') as $file)
          {
          	$pathArr=explode('/',$file);
              array_push($namestore,$pathArr[2]);
          }
          return $namestore;
  	}

   
   
}
