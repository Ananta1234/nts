<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Admin\Banner;
use App\Http\Traits\Imageupload;
class BannerController extends Controller
{
    use Imageupload;
	public function __construct()
	{
		$this->banner=new Banner();
	}


    public function index()
    {
        $banners=$this->banner->getLatestBanner();
        return view('admin.banner.index',compact('banners'));
    }

    public function create()
    {
        return view('admin.banner.form');
    }

    public function store(Request $request)
    {
        $data=$this->requestData();
        if($request->hasFile('image'))
            $data['image']=$this->requestImage();
        $this->banner->storeData($data);
        return back()->with('success','Created Successfully');
    }

    public function show(Banner $banner)
    {
        return view('admin.banner.form',compact('banner'));
    }

    public function update(Banner $banner)
    {
        $data=$this->requestData($banner->id);
        if(request()->hasFile('image'))
            $data['image']=$this->requestImage($banner->image);
        $banner->update($data);
        return redirect(route('banner.index'))->with('success','Updated Successfully');
    }

    public function destroy(Banner $banner)
    {
        if(!empty($banner->image))
        {
            $path=$this->path();
            $this->deleteImage($banner->image,$path);
        }
        $banner->delete();
        return redirect()->route('banner.index')->with('danger','Delete Successfully');
    }

    protected function requestData($banner=null){
        $this->validateFormdata($banner);
        $attributes=request()->except('image');
        return $attributes;

    }

    protected function validateFormdata($banner)
    {
        return request()->validate([
            'title'=>'required|unique:banners,title,'.@$banner,
            'status'=>'nullable',
            'image'=>'nullable|image|max:2048',
            'description' =>'nullable'
        ]);
  
    }

    protected function path()
    {
        return 'images/banners';
    }
    protected function width()
    {
        return 1200;
    }
    protected function height()
    {
        return 550;
    }
   
}
