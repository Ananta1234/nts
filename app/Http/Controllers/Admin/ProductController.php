<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Product;
use App\Models\Admin\Category;
use App\Http\Traits\Imageupload;
  

class ProductController extends Controller
{
	use Imageupload;
	public function __construct()
	{
		$this->product =new Product();
	}

	public function index(Category $category)
	{
		if($category->exists)
			$products=$category->products()->latest()->get();
		else
			$products=$this->product->getLatest();
		return view('admin.product.index',compact('products'));
	}
	public function create()
	{
		return view('admin.product.form');
	}

	public function store(Request $request)
	{
		$data=$this->requestData();
		if($request->hasFile('image'))
			$data['image']=$this->requestImage();
		$this->product->storeData($data);
		return back()->with('success','Created Successfully');

	}

	public function show(Product $product)
	{
		return view('admin.product.form',compact('product'));
	}

	public function update(Product $product)
	{
		$data=$this->requestData($product->id);
		if(request()->hasFile('image'))
			$data['image']=$this->requestImage($product->image);
		$product->update($data);
		return redirect(route('product.index'))->with('success','Updated Successfully');
	}

	public function destroy(Product $product)
	{
		if(!empty($product->image))
		{
			$path=$this->path();
			$this->deleteImage($product->image,$path);
		}
		$product->delete();
		return redirect()->route('product.index')->with('danger','Delete Successfully');
	}

	
	protected function requestData($product=null){
		$this->validateFormdata($product);
		$attributes=request()->except('image');
		$attributes['slug']=$this->slug($attributes['name']);
		return $attributes;

	}

	protected function validateFormdata($product)
    {
    	return request()->validate([
    		'name'=>'required|unique:products,name,'.@$product,
    		'category_id'=>'required',
    		'status'=>'nullable',
    		'image'=>'nullable|image|max:2048',
    		'description' =>'nullable'
    	]);
  
    }
    protected function path()
    {
    	return 'images/products';
    }

    protected function width()
    {
    	return 500;
    }

    protected function height()
    {
    	return 350;
    }

    protected function slug($name)
    {
        return \Str::slug($name);
    }


}
