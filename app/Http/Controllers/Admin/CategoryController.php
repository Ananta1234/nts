<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Admin\Category;

class CategoryController extends Controller
{
	public function __construct()
	{
		$this->category=new Category();
	}
    public function index()
    {
    	return view('admin.category.index');
    }

    public function store()
    {
        $data=$this->requestData();
    	$this->category->storeData($data);
    	return back()->with('success','Create Successfully');
    }

    public function show(Category $category)
    {
        return view('admin.category.index',compact('category'));

    }

    public function update(Category $category)
    {
        $data=$this->requestData($category->id);
        $categories=$category->update($data);
        return redirect(route('category.index'))->with('success','Updated Successfully');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('category.index')->with('danger','Deleted Successfully');
    }

    public function ordering(Request $request)
    {
        if($request->has('ids'))
        {
            $arr=explode(',',$request->input('ids'));
            foreach($arr as $sortOrder=>$id)
            {
                $this->category->whereId($id)->update(['sort'=>$sortOrder]);
            }
            echo json_encode("Category sorting Succesfully");
        }
    }



    protected function validateFormdata($category)
    {
    	return $this->validate(request(),[
    		'name'=>'required|unique:categories,name,'.@$category,
    		'description' =>'nullable',
            'status' =>'nullable'
    	]);
    }

    protected function requestData($category=null)
    {
        $attributes=$this->validateFormdata($category);
        $attributes['slug']=$this->slug($attributes['name']);
        if(empty($category))
            $attributes['sort']=$this->category->order();
        return $attributes;
    }

    protected function slug($name)
    {
        return \Str::slug($name);
    }
   
}
