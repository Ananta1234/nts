<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Models\Admin\Cart;
use Mail;

class CartController extends Controller
{
	public function __construct()
	{
		$this->cart=new Cart();
	}
    public function cart()
    {

    	$sessionId=$this->getSessionId();
    	$cartItem=$this->cart->getSessionCart($sessionId); 
    	return view('front.cart',compact('cartItem'));
    }

    public function cartStore(Request $request)
    {
    	$data['product_id']=$request->product_id;
    	if(Session::has('sessionId'))
    	{
    		$data['session_id']=$this->getSessionId();
    		$countSessionProduct=$this->cart->where(['session_id'=>$data['session_id'],'product_id'=>$data['product_id']])->count();
    		if($countSessionProduct>0)
    			return back()->with('danger','!! Product already exist !!');
    	}
    	else
    	{
    		$data['session_id']=Session::getId();
    		Session::put('sessionId',$data['session_id']);
    	}
    	$this->cart->storeCart($data);
    	return redirect()->route('front.cart')->with('success','Product successfully added');

    }

    public function removeCart(Request $request)
    {
    	$data['session_id']=$this->getSessionId();
    	if(Session::has('sessionId'))
    	{
    		$countSessionId=$this->cart->where('session_id',$data['session_id'])->count();
    		if($countSessionId==1)
    			Session::forget('sessionId');
    		$this->cart->where(['session_id'=>$data['session_id'],'product_id'=>$request->product_id])->delete();
    	}

    	return back()->with('danger','Cart Item deleted successfully');
    }

    public function order(Request $request)
    {
    	$email="anantashrestha01@gmail.com";
    	$data['session_id']=$this->getSessionId();
    	$data['productId']=$this->cart->where('session_id',$data['session_id'])->get();
        $email = "anantashrestha01@gmail.com";
        $messageData=[
          'name'=>$request['full_name'],
          'phone'=>$request['phone'],
          'email'=>$request['email'],
          'address'=>$request['address'],
   		  'product_id'=>$data['productId'],
   		  'qty'=>$request['qty']
        ];
        Mail::send('front.orderemail',$messageData,function($message)use($email){
            $message->to($email)->subject("Order From Customer");
        });
        $this->cart->where('session_id',$data['session_id'])->delete();
        Session::forget('sessionId');
        return back()->with('success','Order has been sent');
    }


    protected function getSessionId()
    {
    	return Session::get('sessionId');
    }
}
