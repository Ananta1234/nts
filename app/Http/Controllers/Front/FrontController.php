<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Banner;
use App\Models\Admin\Category;
use App\Models\Admin\Gallery;
use Mail;
class FrontController extends Controller
{
    public function index()
    {
    	$data['banner']=Banner::where('status',1)->get();
    	return view('front.index',compact('data'));
    }

    public function about()
    {
        return view('front.about');
    }

    public function gallery()
    {
    	$data['galleries']=(new Gallery)->get();
    	return view('front.gallery',compact('data'));
    }

    public function contact()
    {
        return view('front.contact');
    }

    public function contactStore(Request $request)
    {

        $data=$request->all();
        $email = "anantashrestha01@gmail.com";
        $messageData=[
          'name'=>$data['name'],
          'phone'=>$data['phone'],
          'email'=>$data['email'],
          'subject'=>$data['subject'],
          'comment' =>$data['message']
        ];
        Mail::send('front.contact_inquery',$messageData,function($message)use($email){
            $message->to($email)->subject("Enquiry from your website Nts");
        });
        return back()->with('success','Thanks for inquiry.We will contact you soon as possible !!');
    }

}
