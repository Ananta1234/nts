<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\Models\Admin\Product;

class FrontproductController extends Controller
{
    public function categoryproduct(Category $category)
    {
    	return view('front.categoryproduct',compact('category'));
    }

    public function productdetails(Product $product)
    {
    	return view('front.products.productdetails',compact('product'));
    }

    public function search(Request $request)
    {
        $search_product=$request->product;
        $products=(new Product)->where('name','like','%'.$search_product.'%')->where('status','1')->get();
        return view('front.products.searchproduct',compact('products','search_product'));

    }

}
