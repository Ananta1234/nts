<?php
use App\Models\Admin\Category;
use App\Models\Admin\Setting;
use App\Models\Admin\Social;

if(!function_exists('getOrderCategory'))
{
	function getOrderCategory()
	{
		return Category::orderBy('sort','asc')->get();
	}
	
	
}
if(!function_exists('getOrderCategoryActive'))
{
	function getOrderCategoryActive()
	{
		return Category::where('status',1)->orderBy('sort','asc')->get();
	}
	
	
}
if(!function_exists('getSetting'))
{
	function getSetting()
	{
		return Setting::first();
	}
	
	
}
if(!function_exists('getSocialIcon'))
{
	function getSocialIcon()
	{
		$socials=Social::get();
		$iconName=[];
		foreach($socials as $social)
		{
			$socialArr=explode('.',$social->social);
			$iconName[$socialArr[1]]=$social->social;
		}
		return $iconName;
	}
	
	
}