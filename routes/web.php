<?php

use Illuminate\Support\Facades\Route;
//frontController
use App\Http\Controllers\Front\FrontController;
use App\Http\Controllers\Front\FrontproductController;
use App\Http\Controllers\Front\CartController;


//adminController
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\GalleryController;

use App\Http\Controllers\Admin\SettingController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('clear',function(){
	\Artisan::call('config:cache');
	\Artisan::call('cache:clear');
	\Artisan::call('view:clear');
	\Artisan::call('route:clear');
	return back();
});

Route::get('/',[FrontController::class,'index'])->name('front.index');
Route::get('about',[FrontController::class,'about'])->name('front.about');

Route::get('gallery',[FrontController::class,'gallery'])->name('front.gallery');
Route::get('contact',[FrontController::class,'contact'])->name('front.contact');
Route::post('contactStore',[FrontController::class,'contactStore'])->name('front.contactStore');
Route::get('product/{category}',[FrontproductController::class,'categoryproduct'])->name('front.product');
Route::get('productdetails/{product}',[FrontproductController::class,'productdetails'])->name('front.productdetails');
Route::post('search',[FrontproductController::class,'search'])->name('front.search');

Route::get('cart',[CartController::class,'cart'])->name('front.cart');
Route::post('cartStore',[CartController::class,'cartStore'])->name('front.cartStore');
Route::post('removeCart',[CartController::class,'removeCart'])->name('front.removeCart');
Route::post('order',[CartController::class,'order'])->name('front.order');

/*B_[nhb^oaM4E*/






Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	Route::get('dashboard',[AdminController::class,'index'])->name('admin.index');
	Route::get('changePassword',[AdminController::class,'changePass'])->name('admin.changepassword');
	Route::post('changePassword',[AdminController::class,'change']);
	//categorycontroller
	Route::get('category',[CategoryController::class,'index'])->name('category.index');
	Route::post('category',[CategoryController::class,'store'])->name('category.store');
	Route::get('category/{category}',[CategoryController::class,'show'])->name('category.show');
	Route::patch('category/{category}',[CategoryController::class,'update'])->name('category.update');
	Route::get('category/destroy/{category}',[CategoryController::class,'destroy'])->name('category.destroy');
	Route::post('/categorySort',[CategoryController::class,'ordering'])->name('categorySort');

	//productcontroller
	Route::get('product/{category?}',[ProductController::class,'index'])->name('product.index');
	Route::get('product/form/create',[ProductController::class,'create'])->name('product.create');
	Route::post('product/create',[ProductController::class,'store'])->name('product.store');
	Route::get('product/form/{product}',[ProductController::class,'show'])->name('product.show');
	Route::patch('product/{product}',[ProductController::class,'update'])->name('product.update');
	Route::get('product/destroy/{product}',[ProductController::class,'destroy'])->name('product.destroy');

	//banner
	Route::get('banner',[BannerController::class,'index'])->name('banner.index');
	Route::get('banner/form/create',[BannerController::class,'create'])->name('banner.create');
	Route::post('banner/create',[BannerController::class,'store'])->name('banner.store');
	Route::get('banner/form/{banner}',[BannerController::class,'show'])->name('banner.show');
	Route::patch('banner/{banner}',[BannerController::class,'update'])->name('banner.update');
	Route::get('banner/destroy/{banner}',[BannerController::class,'destroy'])->name('banner.destroy');

	//gallery 

	Route::get('gallery',[GalleryController::class,'index'])->name('gallery.index');
	Route::post('gallery',[GalleryController::class,'store'])->name('gallery.store');
	Route::post('uploadFile',[GalleryController::class,'uploadImage'])->name('uploadFile');
	Route::post('removeImage',[GalleryController::class,'removePreview'])->name('removeImage');
	Route::post('deleteImage',[GalleryController::class,'destroy'])->name('gallery.destroy');


	//setting

	Route::get('setting',[SettingController::class,'index'])->name('setting.index');
	Route::post('setting',[SettingController::class,'store'])->name('setting.store');
	Route::post('removeSocial',[SettingController::class,'deleteSocial'])->name('removeLinks');
});
Auth::routes();

/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/
